<?php
/**
 * Copyright © MageClass Consulting (https://www.pronkoconsulting.com)
 * See LICENSE for the license details.
 */

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::THEME,
    'frontend/MageClass/ClickAndCollect',
    __DIR__
);
